#!/bin/sh
apt-get update

apt-get install docker -y
apt-get install systemctl -y
apt-get install python3-scrapy -y
apt-get install python3-pymysql -y
apt-get install python3-termcolor -y
apt-get install python3-virtualenv -y
apt-get install python3-mysqldb -y

