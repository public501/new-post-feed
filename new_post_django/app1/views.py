from django.shortcuts import render
from django.http import HttpResponse
from app1.models import MainTable
from django.template import loader


# Create your views here.

def index(request):
    head_list = MainTable.objects.values_list('header')
    sum_list = MainTable.objects.values_list('summary')
    link_list = MainTable.objects.values_list('link')

    head_iter = iter(head_list)
    sum_iter = iter(sum_list)
    link_iter = iter(link_list)



    template = loader.get_template('app1/index.html')
    context = {
        'head_list': head_list,
        'sum_list': sum_list,
        'link_list': link_list,

        'head_iter': head_iter,
        'sum_iter': sum_iter,
        'link_iter': link_iter,
    }

    return HttpResponse(template.render(context, request))




