from django import template

register = template.Library()

@register.simple_tag
def returnNext(iterlist):
    return next(iterlist)[0]

@register.simple_tag
def returnNextLink(iterlist):
   # a = "https://www.sciencedaily.com"+next(iterlist)[0] unneeded now as this step is in main.py
    return next(iterlist)[0]