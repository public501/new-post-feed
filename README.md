# new-post-feed

Periodically scrapes a website and notifies us if there is a new change, through a python generated graphical window or django based web app.
For now I'm using a random bio website, though it might be there for the long run.

## Use docker compose but just in case, to setup docker manually do
Install and run mysql image:<br>
```sudo docker pull mysql/mysql-server:latest ```<br>
```sudo docker run --name=mysql1 -d mysql/mysql-server```<br><br>

Change one time root password: <br>
``` sudo docker logs [container_name] ```<br>
Copy the password from the logs, open the interactive terminal, login to the sql command line and execute:<br>
```  ALTER USER 'root'@'localhost' IDENTIFIED BY '[newpassword]'; ``` to change the password. <br><br>

Interactive terminal:<br>
```docker exec -it mysql1 bash``` <br><br>
SQL command line:<br>
```  mysql -uroot -p ``` then enter the password when prompted.<br>

# Setup

## 1. Download packages

``` sudo packagedownloader.sh ```<br><br>

## 2. Setup docker

``` docker-compose up ```<br><br>

## 3. Create new mysql user for current device(WSL) and create program db

``` sudo docker exec -it mycontainername bash ``` <br>
``` CREATE USER 'newusername'@'ip.of.wsl.terminal' IDENTIFIED BY 'mypassword'; ```<br>
``` GRANT ALL PRIVILEGES ON *.* TO  'newusername'@'ip.of.wsl.terminal' WITH GRANT OPTION; ```<br>
``` CREATE DATABASE newpostdb; ```<br><br>


## 4. Setup auth.json file
Open ```auth_template.json``` and add the new user and database's details.<br>
Rename the file to ```auth.json```<br>
Copy the file to the ``` new_post_django ``` directory to replace the template there<br><br>


## 4. Crawl website

``` scrapy crawl site1 ```<br>
``` scrapy crawl site2 ```<br><br>

## 5. Update database

``` python3 main.py ```<br><br>

## 6. Run Django development server

``` cd new_post_django ```<br>
``` python3 manage.py runserver```<br><br>


# Troubleshooting

Always make sure that docker and systemctl are installed and running.<br><br>


If there are any errors running the daemon, try:<br>
``` sudo dockerd ```
<br><br>
There seems to be an error with docker not running automatically, unsure if the problem is the specific device being used or whether it is a problem with WSL as a whole. Attempts to fix it have failed, so the only solution is to do it manually:<br>
```sudo dockerd```<br>
Then in another tab/window do:<br>
``` sudo docker-compose up ``` or another command to start the docker container(s)<br>
Now it should all be up and running.<br><br>
Django and other python packages may not work, so in that case a virtual environment needs to be created using virtualenv.
Activate the environment and then run everything.


## WIP/TODO
-Add another site to the project
