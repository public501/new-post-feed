import scrapy
import os
import json
import pickle

#IMPORTANT: when running scrapy crawl site2 do not do so in the scrapy file but in the parent directory(new-post-feed)


outputpath = (str(os.getcwd())).replace('\\', '/')

class Site2Spider(scrapy.Spider):
    name = 'site2'
    allowed_domains = ['scitechdaily.com']
    start_urls = ['http://scitechdaily.com/']

    def parse(self, response):
        #Title
        html_title = response.css('#main-content div.archive-list.mh-section.mh-group article.content-list div.content-thumb a::attr(title)').getall()
        #Loop to fix unicode values us in main.py
        '''for i in range(len(html_title)):
            string_encode = html_title[i].encode("ascii", "ignore")
            string_decode = string_encode.decode()
            html_title[i] = string_decode'''
        


        #Link
        html_link = response.css('#main-content div.archive-list.mh-section.mh-group article.content-list div.content-thumb a::attr(href)').getall()
        
        #Pub date
        html_date = response.css('#main-content div.archive-list header p span.entry-meta-date::text').getall()

        #Summary 
        html_sum = response.css('#main-content div.archive-list article.content-list div.content-list-excerpt p::text').getall()

        mydict = {
            'datelist':html_date,
            'link list':html_link,
            'headlist':html_title,
            'summary list':html_sum
        }


        with open(outputpath+'/site2.json', 'w', encoding='utf-8') as f:
            json.dump(mydict, f, indent=4)
