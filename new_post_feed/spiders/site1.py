import scrapy
import pathlib
import re
import json
import os

global head_ls, sum_ls, link_ls

#IMPORTANT: when running scrapy crawl site1 do not do so in the scrapy file but in the parent directory(new-post-feed)


path_to_self = str(pathlib.Path(__file__).parent.absolute())
q_list = []
outputpath = (str(os.getcwd())).replace('\\', '/') #Replace was there as i was working in windows,
                                                   # switched to wsl now but ill keep it there just in case.



#Takes data as string
def data_clean(element):   
    special_list = ["\n" , "\t" , "\r" , "\f"]
    element = re.sub("[^a-zA-Z\d\s]" , "" , element)
    for i in special_list:
        if i in element:
            element.replace(i, "")

    return(element)

class site1Spider(scrapy.Spider):
    name = "site1"
    start_urls = [
        'https://www.sciencedaily.com/news/plants_animals/biology/'
        ]
    

    # parse() will handle the requests of the urls
    # even when we don't explicitly tell it to do so
    def parse(self, response):
        head_ls = response.css('div.container div.row div.col-sm-8 div.row div.col-md-5 div.hero div.tab-pane h3.latest-head a::text').getall()
        sum_ls = response.css('div.container div.row div.col-sm-8 div.row div.col-md-5 div.hero div.tab-pane div.latest-summary::text').getall()
        link_ls = response.css('div.container div.row div.col-sm-8 div.row div.col-md-5 div.hero div.tab-pane h3.latest-head a::attr(href)').getall()
        
        count = 0
        for i in sum_ls:
            j = data_clean(i)           
            sum_ls[count] = j
            count = count+1
        
        # Quick loop to delete missed new line elements
        for i in sum_ls:
            if i == '\n':
                sum_ls.remove(i)

        for i in link_ls:
            link_ls[i] = 'https://www.sciencedaily.com'+i

        mydict = {
            'headlist':head_ls,
            'summary list':sum_ls,
            'link list' :link_ls
        }


        with open(outputpath+'/site1.json', 'w', encoding='utf-8') as f:
            json.dump(mydict, f, indent=4)

