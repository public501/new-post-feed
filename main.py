import pymysql
import json
import os
import time
from termcolor import colored


# General variables
current_time = int(time.time())
minute_epoch = 60
test_epoch = 2
day_epoch = 86400
outputpath = (str(os.getcwd() + "/")).replace("\\", "/")
datafile1 = outputpath + "site1.json"
datafile2 = outputpath + "site2.json"

# Queries (begin with a q)
qtable_updatetime = " SELECT UNIX_TIMESTAMP(UPDATE_TIME) FROM information_schema.tables WHERE TABLE_SCHEMA = 'newpostdb' AND TABLE_NAME = 'main_table'; "
qtable_createtime = " SELECT UNIX_TIMESTAMP(CREATE_TIME) FROM information_schema.tables WHERE TABLE_SCHEMA = 'newpostdb' AND TABLE_NAME = 'main_table'; "
qrows_mod_epoch = "SELECT UNIX_TIMESTAMP(timestamp) FROM main_table;"
qmain_table_exist = "select max(case when table_name = 'main_table' then 1 else 0 end) as TableExists from information_schema.tables;"
qdrop_old = "DROP TABLE old_table"
qrename_main = "RENAME TABLE main_table TO old_table;"
qcreate_main = "CREATE TABLE main_table (id int primary key auto_increment ,header text, summary text, link text, timestamp timestamp not null default current_timestamp on update current_timestamp)"

# Authentication
with open(outputpath + "auth.json", "r") as f:
    auth = json.load(f)
    hostname = auth["sql"]["host"]
    username = auth["sql"]["user"]
    passwd = auth["sql"]["pass"]
    mydatabase = auth["sql"]["db"]


def connection():
    # Establish connection
    conn = pymysql.connect(
    host=hostname,
    user=username,
    password=passwd,
    db=mydatabase,
    )
    #Cursor
    curr = conn.cursor()

    return conn, curr

class main_site:
    def read_file1(datafile):
        # Read (new) data
        with open(datafile, "r", encoding="utf-8") as f:
            data = json.load(f)
        return data

    def write_to_table(filedata):
        # Write the data to the table
        for i in range(len(filedata["headlist"])):
            single_head = filedata["headlist"][i]
            single_sum = filedata["summary list"][i]
            single_link = filedata["link list"][i]

            query = f'insert into main_table (header, summary, link) values ("{single_head}", "{single_sum}", "{single_link}");'
            curr.execute(query)
            conn.commit()

        print(colored("All data successfully written to table.", "green"))
    

    def update_table1():

        # Does the main table exist?
        curr.execute(qmain_table_exist)
        main_table_exist = curr.fetchone()[0]
        print("\n\n")
        print(main_table_exist)

        # If it does exist do:
        if main_table_exist == 1:  # if-1
            # Does it need updating?
            curr.execute(qtable_updatetime)

            try:
                table_updatetime = curr.fetchall()[0][0]
            except:
                print(
                    colored(
                        "There has been an error getting the updatetime i'll just assume it does need updating.",
                        "red",
                    )
                )
                table_updatetime = 86400

            if table_updatetime == None:  # if-2
                print("innodb bug gave me null update_time\nusing create time instead")
                curr.execute(qtable_createtime)
                table_updatetime = curr.fetchall()[0][0]
                print(table_updatetime)

            # If table does need updating:
            print(
                "current_time-table_updatetime: " + str(current_time - table_updatetime)
            )
            if (current_time - table_updatetime) >= day_epoch:  # if-3

                print("The table needs updating.")
                print("Dropping/deleting the old table...")
                # Try except used here in case the old table doesnt exist
                try:
                    curr.execute(qdrop_old)
                except:
                    print(
                        colored(
                            "The old table doesnt exist. Skipping the drop step.\nIf the table does exist then there is an error that needs fixing.",
                            "red",
                        )
                    )

                print("Renaming the main table to old table...")
                curr.execute(qrename_main)

                print("Creating the new main table...")
                curr.execute(qcreate_main)

                #Write the data to the table
                main_site.write_to_table(data1)
                main_site.write_to_table(data2)


            else:  # if-3
                print(
                    colored(
                        "table doesnt need updating or somethings broken", "magenta"
                    )
                )

        elif main_table_exist == 0:  # if-1
            print("Main table does not exist and is being created.")
            curr.execute(qcreate_main)
            print(colored("Successfully created table.", "green"))

            # Write the data to the table
            main_site.write_to_table(data1)
            main_site.write_to_table(data2)

        else:
            print(
                colored(
                    "There is a problem with the main_table_exist:\n"
                    + main_table_exist,
                    "red",
                )
            )




# To get truely up to date results run 'scrapy crawl site1' in the new-post-feed dir
if __name__ == "__main__":
    conn,curr = connection()

    data1 = main_site.read_file1(datafile1)
    data2 = main_site.read_file1(datafile2)
    main_site.update_table1()
